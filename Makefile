LIB_NAME = libhermes.a
LIB_SRCFILES = $(wildcard source/*.cpp)
LIB_OBJFILES = $(LIB_SRCFILES:%.cpp=%.o)

EXT_MODULE_NAME = libhmsextmodule.a
EXT_MODULE_SRCFILES = ext/hmsModule.cpp
EXT_MODULE_OBJFILES = $(EXT_MODULE_SRCFILES:%.cpp=%.o)

EXT_JNI_NAME = libhmsextjni.a
EXT_JNI_SRCFILES = ext/hmsJNI.cpp
EXT_JNI_OBJFILES = $(EXT_JNI_SRCFILES:%.cpp=%.o)

EXT_AASSETREADER_NAME = libhmsextaassetreader.a
EXT_AASSETREADER_SRCFILES = ext/hmsAAssetReader.cpp
EXT_AASSETREADER_OBJFILES = $(EXT_AASSETREADER_SRCFILES:%.cpp=%.o)

EXT_SERIALIZER_NAME = libhmsextserializer.a
EXT_SERIALIZER_SRCFILES = ext/hmsSerializer.cpp
EXT_SERIALIZER_OBJFILES = $(EXT_SERIALIZER_SRCFILES:%.cpp=%.o)

EXT_GZIPREADER_NAME = libhmsextgzipreader.a
EXT_GZIPREADER_SRCFILES = ext/hmsGZipReader.cpp
EXT_GZIPREADER_OBJFILES = $(EXT_GZIPREADER_SRCFILES:%.cpp=%.o)

EXT_AUDIO_NAME = libhmsextaudio.a
EXT_AUDIO_SRCFILES = ext/hmsAudio.cpp
EXT_AUDIO_OBJFILES = $(EXT_AUDIO_SRCFILES:%.cpp=%.o)

EXT_AUDIO_C_NAME := libhmsextaudio_c.
EXT_AUDIO_C_SRCFILES = ext/bridge_c/hmsAudioC.cpp
EXT_AUDIO_C_OBJFILES = $(EXT_AUDIO_C_SRCFILES:%.cpp=%.o)
EXT_AUDIO_C_LDFLAGS =
EXT_AUDIO_C_LDLIBS = -lhmsextaudio
ifndef APPLE
	EXT_AUDIO_C_NAME := $(EXT_AUDIO_C_NAME)so
	EXT_AUDIO_C_LDFLAGS := -Wl,-soname,libhmsextaudio_c.so -Wl,-rpath=.
ifdef ANDROID
	EXT_AUDIO_C_LDLIBS += -landroid
endif
else
	EXT_AUDIO_C_NAME := $(EXT_AUDIO_C_NAME)dylib
	EXT_AUDIO_C_LDFLAGS := -Wl,-dylib -Wl,-install_name,hmsextaudio_c
	EXT_AUDIO_C_LDLIBS += -framework AudioToolbox -framework AVFoundation -framework CoreFoundation -framework Foundation
endif

CXXFLAGS += -Iinclude -Idepend/aes/include -Idepend/curl/include -Idepend/jsoncpp/include -Idepend/miniaudio -Idepend/zlib/include -Wreorder

ifndef NDEBUG
    CXXFLAGS += -g -D_DEBUG=1 -DDEBUG=1
else
    ifdef DSYM
        CXXFLAGS += -g
    endif
    CXXFLAGS += -DNDEBUG=1 -O2
endif

.PHONY: all clean

all: $(LIB_NAME) $(EXT_MODULE_NAME) $(EXT_SERIALIZER_NAME) $(EXT_GZIPREADER_NAME) $(EXT_AUDIO_NAME)

$(LIB_NAME): $(LIB_OBJFILES)
	$(AR) rs $@ $^

$(EXT_MODULE_NAME): $(EXT_MODULE_OBJFILES)
	$(AR) rs $@ $^

$(EXT_JNI_NAME): $(EXT_JNI_OBJFILES)
	$(AR) rs $@ $^
	
$(EXT_AASSETREADER_NAME): $(EXT_AASSETREADER_OBJFILES)
	$(AR) rs $@ $^

$(EXT_SERIALIZER_NAME): $(EXT_SERIALIZER_OBJFILES)
	$(AR) rs $@ $^

$(EXT_GZIPREADER_NAME): $(EXT_GZIPREADER_OBJFILES)
	$(AR) rs $@ $^

$(EXT_AUDIO_NAME): $(EXT_AUDIO_OBJFILES)
	$(AR) rs $@ $^

$(EXT_AUDIO_C_NAME): $(EXT_AUDIO_C_OBJFILES)
	$(CXX) $(LDFLAGS) $(EXT_AUDIO_C_LDFLAGS) $^ $(EXT_AUDIO_C_LDLIBS) -o $@

%.d:%.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MM -MF $@ $<

clean:
ifeq ($(OS), Windows_NT)
	del $(LIB_NAME) $(EXT_MODULE_NAME) $(EXT_JNI_NAME) $(EXT_AASSETREADER_NAME) $(EXT_SERIALIZER_NAME) $(EXT_GZIPREADER_NAME) $(EXT_AUDIO_NAME) $(EXT_AUDIO_C_NAME) source\*.o source\*.d ext\*.o ext\*.d ext\bridge_c\*.o ext\bridge_c\*.d >nul 2>&1
else
	$(RM) $(LIB_NAME) $(EXT_MODULE_NAME) $(EXT_JNI_NAME) $(EXT_AASSETREADER_NAME) $(EXT_SERIALIZER_NAME) $(EXT_GZIPREADER_NAME) $(EXT_AUDIO_NAME) $(EXT_AUDIO_C_NAME) source/*.o source/*.d ext/*.o ext/*.d ext/bridge_c/*.o ext/bridge_c/*.d
endif
