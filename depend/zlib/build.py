import glob
import os
import shutil
import sys
sys.path.append(os.path.join('..', '..', 'script'))
from build_common import *

libraryVersion = '1.3'
libraryName = 'zlib-' + libraryVersion

settings = Settings()

if configure(settings, os.path.join('..', '..')):
    remove(libraryName)
    if downloadAndExtract('https://github.com/madler/zlib/releases/download/v' + libraryVersion + '/' + libraryName + '.tar.gz', '', libraryName + '.tar.gz', ''):
        remove('include')
        shutil.copy2('Makefile.in', os.path.join(libraryName, 'Makefile'))
        os.chdir(libraryName)
        with open('gzguts.h', 'a') as f:
            f.write('\n#include <unistd.h>\n')

        if buildMake(['zlib'], settings, ''):
            pathInclude = os.path.join('..', 'include')
            os.makedirs(pathInclude)
            for v in glob.glob(r'*.h'):
                shutil.copy(v, pathInclude)
        os.chdir('..')
        remove(libraryName)
