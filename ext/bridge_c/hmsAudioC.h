// Copyright (C) 2023-2024 Patryk Nadrowski
// This file is part of the "Hermes" library.
// For conditions of distribution and use, see copyright notice in license.txt.

#ifndef _HMS_AUDIO_C_H_
#define _HMS_AUDIO_C_H_

#include <stdbool.h>
#include <stdint.h>
#if defined(ANDROID) || defined(__ANDROID__)
#include <android/asset_manager.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif
__attribute__ ((__visibility__ ("default"))) bool hms_ext_AudioManager_initialize(uint32_t pLimit);
__attribute__ ((__visibility__ ("default"))) void hms_ext_AudioManager_terminate();

__attribute__ ((__visibility__ ("default"))) bool hms_ext_AudioManager_add(uint32_t pId, const char* pFileName, bool pMusic);
__attribute__ ((__visibility__ ("default"))) void hms_ext_AudioManager_remove(uint32_t pId);

__attribute__ ((__visibility__ ("default"))) bool hms_ext_AudioManager_play(uint32_t pId);
__attribute__ ((__visibility__ ("default"))) bool hms_ext_AudioManager_pause(uint32_t pId);
__attribute__ ((__visibility__ ("default"))) bool hms_ext_AudioManager_stop(uint32_t pId);
__attribute__ ((__visibility__ ("default"))) bool hms_ext_AudioManager_loop(uint32_t pId, bool pLoop);

__attribute__ ((__visibility__ ("default"))) void hms_ext_AudioManager_pauseAll();
__attribute__ ((__visibility__ ("default"))) void hms_ext_AudioManager_resumeAll();

#if defined(ANDROID) || defined(__ANDROID__)
__attribute__ ((__visibility__ ("default"))) AAssetManager* hms_ext_AudioManager_assetManagerGet();
__attribute__ ((__visibility__ ("default"))) void hms_ext_AudioManager_assetManagerSet(AAssetManager* pAssetManager);
#endif
#ifdef __cplusplus
};
#endif
#endif
