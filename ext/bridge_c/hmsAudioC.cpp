// Copyright (C) 2023-2024 Patryk Nadrowski
// This file is part of the "Hermes" library.
// For conditions of distribution and use, see copyright notice in license.txt.

#include "hmsAudioC.h"
#include "../hmsAudio.hpp"

#include <cassert>

bool hms_ext_AudioManager_initialize(uint32_t pLimit)
{
    assert(pLimit > 0);
    return hms::ext::AudioManager::instance().initialize(pLimit);
}

void hms_ext_AudioManager_terminate()
{
    hms::ext::AudioManager::instance().terminate();
}

bool hms_ext_AudioManager_add(uint32_t pId, const char* pFileName, bool pMusic)
{
    assert(pFileName != nullptr);
    return hms::ext::AudioManager::instance().add(pId, pFileName, pMusic);
}

void hms_ext_AudioManager_remove(uint32_t pId)
{
    hms::ext::AudioManager::instance().remove(pId);
}

bool hms_ext_AudioManager_play(uint32_t pId)
{
    return hms::ext::AudioManager::instance().play(pId);
}

bool hms_ext_AudioManager_pause(uint32_t pId)
{
    return hms::ext::AudioManager::instance().pause(pId);
}

bool hms_ext_AudioManager_stop(uint32_t pId)
{
    return hms::ext::AudioManager::instance().stop(pId);
}

bool hms_ext_AudioManager_loop(uint32_t pId, bool pLoop)
{
    return hms::ext::AudioManager::instance().loop(pId, pLoop);
}

void hms_ext_AudioManager_pauseAll()
{
    hms::ext::AudioManager::instance().pauseAll();
}

void hms_ext_AudioManager_resumeAll()
{
    hms::ext::AudioManager::instance().resumeAll();
}

#if defined(ANDROID) || defined(__ANDROID__)
AAssetManager* hms_ext_AudioManager_assetManagerGet()
{
    return hms::ext::AudioManager::instance().assetManager();
}

void hms_ext_AudioManager_assetManagerSet(AAssetManager* pAssetManager)
{
    hms::ext::AudioManager::instance().assetManager(pAssetManager);
}
#endif
