// Copyright (C) 2023-2024 Patryk Nadrowski
// This file is part of the "Hermes" library.
// For conditions of distribution and use, see copyright notice in license.txt.

#include "hmsAudio.hpp"

#define MINIAUDIO_IMPLEMENTATION
#define MA_NO_AAUDIO // AAUDIO is bugged on some weaker phones, so better stay with OpenSL
#include "miniaudio.h"

#include <cassert>
#include <functional>
#include <limits>
#include <vector>

namespace {
    void MA_DEVICE_CALLBACK(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 pFramesCount) {
        const auto callback = static_cast<std::function<ma_uint64(float*, ma_uint32, ma_uint32)>*>(pDevice->pUserData);
        const auto total = (*callback)(static_cast<float*>(pOutput), pFramesCount, pDevice->playback.channels);
        if (total < pFramesCount) {
            ma_silence_pcm_frames(ma_offset_pcm_frames_ptr(pOutput, total, pDevice->playback.format, pDevice->playback.channels), (pFramesCount - total), pDevice->playback.format, pDevice->playback.channels);
        }
    }

#if defined(ANDROID) || defined(__ANDROID__)
    AAssetManager* gAssetManager = nullptr;

    ma_result MA_VFS_OPEN_ANDROID(ma_vfs* pVFS, const char* pPath, ma_uint32 pMode, ma_vfs_file* pFile) {
        if (!gAssetManager) {
            return MA_UNAVAILABLE;
        } else if (pFile == nullptr) {
            return MA_INVALID_ARGS;
        }

        auto asset = AAssetManager_open(gAssetManager, pPath, AASSET_MODE_STREAMING);
        if (!asset)
            return MA_DOES_NOT_EXIST;

        *pFile = static_cast<ma_vfs_file>(asset);

        return MA_SUCCESS;
    }

    ma_result MA_VFS_CLOSE_ANDROID(ma_vfs* pVFS, ma_vfs_file pFile) {
        if (pFile == nullptr) {
            return MA_INVALID_ARGS;
        }

        AAsset_close(static_cast<AAsset*>(pFile));

        return MA_SUCCESS;
    }

    ma_result MA_VFS_READ_ANDROID(ma_vfs* pVFS, ma_vfs_file pFile, void* pDestination, size_t pBytesCount, size_t* pBytesRead) {
        if (pFile == nullptr) {
            return MA_INVALID_ARGS;
        }

        ma_result result = MA_SUCCESS;
        const auto bytesRead = AAsset_read(static_cast<AAsset*>(pFile), pDestination, pBytesCount);

        if (pBytesRead) {
            *pBytesRead = bytesRead;
        }

        if (bytesRead == 0) {
            result = MA_AT_END;
        } else if (bytesRead < 0) {
            result = MA_ERROR;
        }

        return result;
    }

    ma_result MA_VFS_SEEK_ANDROID(ma_vfs* pVFS, ma_vfs_file pFile, ma_int64 pOffset, ma_seek_origin pOrigin) {
        if (pFile == nullptr) {
            return MA_INVALID_ARGS;
        }

        int whence = 0;
        if (pOrigin == ma_seek_origin_start) {
            whence = SEEK_SET;
        } else if (pOrigin == ma_seek_origin_end) {
            whence = SEEK_END;
        } else {
            whence = SEEK_CUR;
        }

        const auto offset = AAsset_seek(static_cast<AAsset*>(pFile), pOffset, whence);

        return offset != -1 ? MA_SUCCESS : MA_ERROR;
    }

    ma_result MA_VFS_TELL_ANDROID(ma_vfs* pVFS, ma_vfs_file pFile, ma_int64* pCursor) {
        if (pCursor == nullptr) {
            return MA_INVALID_ARGS;
        }
        *pCursor = 0;
        if (pFile == NULL) {
            return MA_INVALID_ARGS;
        }

        auto asset = static_cast<AAsset*>(pFile);
        *pCursor = AAsset_getLength(asset) - AAsset_getRemainingLength(asset);

        return MA_SUCCESS;
    }

    ma_result MA_VFS_INFO_ANDROID(ma_vfs* pVFS, ma_vfs_file pFile, ma_file_info* pInfo) {
        if (pInfo == nullptr) {
            return MA_INVALID_ARGS;
        }
        MA_ZERO_OBJECT(pInfo);
        if (pFile == NULL) {
            return MA_INVALID_ARGS;
        }

        pInfo->sizeInBytes = AAsset_getLength(static_cast<AAsset*>(pFile));

        return MA_SUCCESS;
    }

    ma_vfs_callbacks gVFSAndroid = {
        MA_VFS_OPEN_ANDROID,
        nullptr,
        MA_VFS_CLOSE_ANDROID,
        MA_VFS_READ_ANDROID,
        nullptr,
        MA_VFS_SEEK_ANDROID,
        MA_VFS_TELL_ANDROID,
        MA_VFS_INFO_ANDROID
    };
#endif
}

namespace hms::ext {
    class AudioManager::Private {
    public:
        class Sound {
        private:
            ma_sound mSound = {};
            bool mValid = false;

        public:
            explicit Sound(ma_engine* pEngine, const std::string& pFilePath, bool pStreaming) {
                auto result = ma_sound_init_from_file(pEngine, pFilePath.c_str(), pStreaming ? MA_SOUND_FLAG_STREAM : 0, nullptr, nullptr, &mSound);
                mValid = result == MA_SUCCESS;
            }

            ~Sound() {
                if (mValid) {
                    ma_sound_uninit(&mSound);
                }
            }

            ma_sound* sound() {
                return &mSound;
            }

            bool valid() const {
                return mValid;
            }
        };

    private:
        ma_engine mEngine = {};
        ma_resource_manager mResourceManager = {};
        bool mValid = false;

    public:
        std::vector<std::unique_ptr<Sound>> mSounds;
        std::vector<uint32_t> mSoundsPaused;

        Private(size_t pLimit) : mSounds(pLimit) {
            auto resourceManagerConfig = ma_resource_manager_config_init();
#if defined(ANDROID) || defined(__ANDROID__)
            resourceManagerConfig.pVFS = &gVFSAndroid;
#endif
            auto result = ma_resource_manager_init(&resourceManagerConfig, &mResourceManager);

            if (result == MA_SUCCESS) {
                auto engineConfig = ma_engine_config_init();
                engineConfig.pResourceManager = &mResourceManager;
                result = ma_engine_init(&engineConfig, &mEngine);

                if (result == MA_SUCCESS) {
                    mValid = true;
                } else {
                    ma_resource_manager_uninit(&mResourceManager);
                }
            }
        }

        ~Private() {
            if (mValid) {
                mSounds.clear();
                ma_engine_uninit(&mEngine);
                ma_resource_manager_uninit(&mResourceManager);
            }
        }

        ma_engine* engine() {
            return &mEngine;
        }

        void start() {
            if (mValid) {
                ma_engine_start(&mEngine);
            }
        }

        void stop() {
            if (mValid) {
                ma_engine_stop(&mEngine);
            }
        }

        bool valid() const {
            return mValid;
        }
    };

    AudioManager::AudioManager() : mLimit(0), mPrivate(nullptr) {
    }

    AudioManager::~AudioManager() {
    }

    bool AudioManager::initialize(uint32_t pLimit) {
        bool result = true;

        if (mPrivate == nullptr) {
            mPrivate = std::make_unique<AudioManager::Private>(pLimit);
            result = mPrivate->valid();
        }

        return result;
    }

    void AudioManager::terminate() {
        mPrivate = nullptr;
    }

    bool AudioManager::add(uint32_t pId, const std::string& pFilePath, bool pStreaming) {
#if defined(ANDROID) || defined(__ANDROID__)
        assert(assetManager() != nullptr);
#endif
        assert(mPrivate != nullptr && pId < mPrivate->mSounds.size() && mPrivate->mSounds[pId] == nullptr);
        bool result = false;

        if (mPrivate->valid()) {
            auto sound = std::make_unique<Private::Sound>(mPrivate->engine(), pFilePath, pStreaming);
            if (sound->valid()) {
                mPrivate->mSounds[pId] = std::move(sound);
                result = true;
            }
        }

        return result;
    }

    void AudioManager::remove(uint32_t pId) {
        assert(mPrivate != nullptr && pId < mPrivate->mSounds.size() && mPrivate->mSounds[pId] != nullptr);
        mPrivate->mSounds[pId] = nullptr;
    }

    bool AudioManager::play(uint32_t pId) {
        assert(mPrivate != nullptr && pId < mPrivate->mSounds.size() && mPrivate->mSounds[pId] != nullptr);
        bool result = false;

        if (mPrivate->valid() && mPrivate->mSounds[pId]->valid()) {
            auto& sound = mPrivate->mSounds[pId];
            if (ma_sound_is_playing(sound->sound())) {
                ma_sound_seek_to_pcm_frame(sound->sound(), 0);
            }
            ma_sound_start(mPrivate->mSounds[pId]->sound());
            result = true;
        }

        return result;
    }

    bool AudioManager::pause(uint32_t pId) {
        assert(mPrivate != nullptr && pId < mPrivate->mSounds.size() && mPrivate->mSounds[pId] != nullptr);
        bool result = false;

        if (mPrivate->valid() && mPrivate->mSounds[pId]->valid()) {
            ma_sound_stop(mPrivate->mSounds[pId]->sound());
            result = true;
        }

        return result;
    }

    bool AudioManager::stop(uint32_t pId) {
        assert(mPrivate != nullptr && pId < mPrivate->mSounds.size() && mPrivate->mSounds[pId] != nullptr);
        bool result = false;

        if (mPrivate->valid() && mPrivate->mSounds[pId]->valid()) {
            auto sound = mPrivate->mSounds[pId]->sound();
            ma_sound_stop(sound);
            ma_sound_seek_to_pcm_frame(sound, 0);
            result = true;
        }

        return result;
    }

    bool AudioManager::loop(uint32_t pId, bool pLoop) {
        assert(mPrivate != nullptr && pId < mPrivate->mSounds.size() && mPrivate->mSounds[pId] != nullptr);
        bool result = false;

        if (mPrivate->valid() && mPrivate->mSounds[pId]->valid()) {
            ma_sound_set_looping(mPrivate->mSounds[pId]->sound(), pLoop);
            result = true;
        }

        return result;
    }

    void AudioManager::pauseAll() {
        assert(mPrivate != nullptr);
        mPrivate->stop();
    }

    void AudioManager::resumeAll() {
        assert(mPrivate != nullptr);
        mPrivate->start();
    }

#if defined(ANDROID) || defined(__ANDROID__)
    AAssetManager* AudioManager::assetManager() const {
        return gAssetManager;
    }

    void AudioManager::assetManager(AAssetManager* pAssetManager) {
        gAssetManager = pAssetManager;
    }
#endif

    AudioManager& AudioManager::instance() {
        static AudioManager instance;        
        return instance;
    }

}
