// Copyright (C) 2023-2024 Patryk Nadrowski
// This file is part of the "Hermes" library.
// For conditions of distribution and use, see copyright notice in license.txt.

#ifndef _HMS_AUDIO_HPP_
#define _HMS_AUDIO_HPP_

#include <atomic>
#include <cstdint>
#include <memory>
#include <string>

#if defined(ANDROID) || defined(__ANDROID__)
#include <android/asset_manager.h>
#endif

namespace hms
{
namespace ext
{
    class AudioManager
    {
    public:
        bool initialize(uint32_t pLimit = 16);
        void terminate();

        bool add(uint32_t pId, const std::string& pFilePath, bool pStreaming);
        void remove(uint32_t pId);

        bool play(uint32_t pId);
        bool pause(uint32_t pId);
        bool stop(uint32_t pId);
        bool loop(uint32_t pId, bool pLoop);

        void pauseAll();
        void resumeAll();

#if defined(ANDROID) || defined(__ANDROID__)
        AAssetManager* assetManager() const;
        void assetManager(AAssetManager* pAssetManager);
#endif

		static AudioManager& instance();

    private:
        class Private;

        AudioManager();
        AudioManager(const AudioManager& pOther) = delete;
        AudioManager(AudioManager&& pOther) = delete;
        ~AudioManager();

        uint32_t mLimit;
        std::unique_ptr<Private> mPrivate;
    };
}
}

#endif
