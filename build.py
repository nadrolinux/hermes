import os
import sys
sys.path.append('script')
from build_common import *

settings = Settings()

def appleFlagsUpdate(pLibraryName):
    if settings.mBuildTarget == 'apple':
        for i in range(0, len(settings.mPlatformName)):
            if settings.mPlatformName[i] == 'ios':
                settings.mArchFlagLD[i] += ' -Llib/apple/' + pLibraryName + '.xcframework/ios-arm64'
            elif settings.mPlatformName[i] == 'ios-simulator':
                settings.mArchFlagLD[i] += ' -Llib/apple/' + pLibraryName + '.xcframework/ios-arm64_x86_64-simulator'
            elif settings.mPlatformName[i] == 'macos':
                settings.mArchFlagLD[i] += ' -Llib/apple/' + pLibraryName + '.xcframework/macos-arm64_x86_64'

if configure(settings, ''):
    bridge = False
    for i in range(1, len(sys.argv)):
        if sys.argv[i] == '-bridge':
            bridge = True

    if not bridge:
        libraries = ['hermes', 'hmsextaudio', 'hmsextgzipreader', 'hmsextmodule', 'hmsextserializer']
        if settings.mBuildTarget == 'android':
            libraries.extend(['hmsextjni', 'hmsextaassetreader'])

        buildMake(libraries, settings, '')
    else:
        appleFlagsUpdate('hmsextaudio')

        buildMake(['hmsextaudio_c'], settings, 'APPLE=1' if settings.mBuildTarget == 'apple' else 'ANDROID=1' if settings.mBuildTarget == 'android' else '', False)
